- Ref with callback

        :::javascript
        function CustomTextInput(props) {
            let textInput = null;
            function handleClick() {
              textInput.focus();
            }
            return (
              <div>
                <input
                  type="text"
                  ref={(input) => { textInput = input; }} />
                <input
                  type="button"
                  value="Focus the text input"
                  onClick={handleClick}
                />
              </div>
            );  
        }

- Autocomplete component
        
        :::javascript
        class Autocomplete extends Component {
          get value() {
            return this.refs.inputResort.value;
          }
          set value(inputValue) {
            this.refs.inputResort.value = inputValue;
          }
          render() {
            return (
              <div>
                <input ref="inputResort" type="text" list="tahoe-resorts" />
                <datalist id="tahoe-resorts">
                  {this.props.options.map((opt, i) => <option key={i}>{opt}</option>)}
                </datalist>
              </div>
            );
          }
        }
        <Autocomplete options={tahoeResorts} ref={input => (_resort = input)} />
        